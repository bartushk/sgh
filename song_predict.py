"""Script used for classifying songs.

Features to extract:
    - frequency
    - frame energy
    - bandwidth
    - mel frequency coefficent
    - PERC
"""
import argparse
from collections import namedtuple, defaultdict
import os
import pickle
import librosa
import numpy as np

from sklearn.cluster import KMeans
from sklearn import decomposition

AudioData = namedtuple('AudioData', ['data', 'sample_rate'])
AudioFeatures = namedtuple('AudioFeautures',
                           ['mfccs', 'chroma', 'mel', 'contrast', 'tonnetz', 'song_name'])


def get_feature_array(afeat):
    """Gets an np array of features from  an AudioFeatures structure."""
    return np.concatenate((afeat.mfccs, afeat.chroma, afeat.mel, afeat.contrast, afeat.tonnetz),
                          axis=0)

def extract_features(audio_data, song_name):
    """Extract features from an AudioData object."""
    stft = np.abs(librosa.stft(audio_data.data))
    mfccs = np.mean(
        librosa.feature.mfcc(y=audio_data.data, sr=audio_data.sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(
        librosa.feature.chroma_stft(S=stft, sr=audio_data.sample_rate).T, axis=0)
    mel = np.mean(
        librosa.feature.melspectrogram(audio_data.data, sr=audio_data.sample_rate).T, axis=0)
    contrast = np.mean(
        librosa.feature.spectral_contrast(S=stft, sr=audio_data.sample_rate).T, axis=0)
    tonnetz = np.mean(
        librosa.feature.tonnetz(y=librosa.effects.harmonic(audio_data.data),
                                sr=audio_data.sample_rate).T, axis=0)
    return AudioFeatures(mfccs, chroma, mel, contrast, tonnetz, song_name)

def features_from_dict(features):
    """Creates AudioFeatures from a dict."""
    return AudioFeatures(features['mfccs'], features['chroma'],
                         features['mel'], features['contrast'],
                         features['tonnetz'], features['song_name'])

def read_features_from_file(file_path):
    """Parses audio features from a file."""
    if not os.path.isfile(file_path):
        return None
    features_dict = pickle.load(open(file_path, "rb"))
    return [features_from_dict(x) for x in features_dict]

def write_features_to_file(file_path, features):
    """Writes a list of AudioFeatures to a file as pickle."""
    print 'Writing features to %s.' % file_path
    features_as_dict = [x._asdict() for x in features]
    pickle.dump(features_as_dict, open(file_path, 'w'))

def get_features_from_music_files(dir_path):
    """Reads all files in a directory and generates features from them."""
    features = []
    for file_name in os.listdir(dir_path):
        file_path = os.path.join(dir_path, file_name)
        try:
            data, s_rate = librosa.load(file_path)
        except:
            print 'Could not load %s, skipping.' % file_name
            continue
        print'Generating features for %s.' % file_name
        features.append(extract_features(AudioData(data, s_rate), file_name))
    return features

def run_clustering(args):
    """Trains a model for predicting song genre."""
    features = read_features_from_file(args.feature_file)
    if features is None:
        print 'No feature file, generating features from %s.' % args.song_dir
        features = get_features_from_music_files(args.song_dir)
        write_features_to_file(args.feature_file, features)
    data_set = []
    for feature_set in features:
        data_set.append(get_feature_array(feature_set))

    song_names = [x.song_name for x in features]
    data_set = np.array(data_set)
    if args.csv_out != '':
        np.savetxt(args.csv_out, data_set, delimiter=",")
    pca = decomposition.PCA(n_components=5)
    data_set = pca.fit(data_set).transform(data_set)
    k_means = KMeans(n_clusters=args.num_clusters).fit(data_set)
    results = zip(song_names, k_means.labels_)
    grouped = defaultdict(lambda: [])
    for result in results:
        grouped[result[1]].append(result[0])
    for group, songs in grouped.iteritems():
        print 'Group %s:' % group
        for song in songs:
            song = song[:75] + (song[75:] and '..')
            print '   %s' % song.strip().replace('.mp3', '')
        print ''
        print ''


def main():
    """Called if this is run and not imported.

        If no feature file exists, this program will load all the files in
        song_dir extract features from them and create a feature file. Otherwise
        all the features for training will be loaded from the feature file.
    """
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--song_dir', dest='song_dir', default='assets/training',
                        type=str, help='Directory containing the training mp3 files.')
    parser.add_argument('--feature_file', dest='feature_file', default='features.pickle',
                        type=str, help='File containing all the features.')
    parser.add_argument('--feature_csv_out', dest='csv_out', default='',
                        type=str, help='File containing all the features.')
    parser.add_argument('--n', dest='num_clusters', default=5,
                        type=int, help='Number of song categories to cluster by.')
    args = parser.parse_args()
    run_clustering(args)



if __name__ == "__main__":
    main()
